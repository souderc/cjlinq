using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinqPractice.Tests
{
    [TestClass]
    public class CollectionsUtilitiesTests
    {
        [DataTestMethod]
        [TestCategory("FirstObject")]
        [DataRow(new object[] { 5, 8, 10, -1 })]
        [DataRow(new object[] { "right", "wrong", "still wrong", "Seriously dude?", 0 })]
        public void FirstInteger(object []collection)
        {
            var found = CollectionsUtilities.FirstObject(collection);
            Assert.AreEqual(collection[0], found);
        }

        [TestMethod]
        [TestCategory("FirstObject")]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void FirstNull()
        {
            var collection = new object[] { };
            CollectionsUtilities.FirstObject(collection);
            Assert.Fail("No Exception was thrown.");
        }

        [TestMethod]
        [TestCategory("FirstObject")]
        [ExpectedException(typeof(ArgumentException))]
        public void FirstEmpty()
        {
            var collection = new object[] { "", "3" };
            CollectionsUtilities.FirstObject(collection);
            Assert.Fail("No Exception was thrown.");
        }

        [DataTestMethod]
        [TestCategory("IntegerSquares")]
        [DataRow(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, new int[] { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100 })]
        [DataRow(new int[] { 1, 11, 3, 2, 5, 6, 7, 8, 9, 10, 4 }, new int[] { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121 })]
        [DataRow(new int[] { 1, 11, -3, 2, 5, 6, 7, 8, 9, 10, 4 }, new int[] { 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121 })]
        public void CollectionOfIntegers(int[] collection, int[] expected)
        {
            var returned = CollectionsUtilities.IntegerCollection(collection);
            CollectionAssert.AreEqual(expected, returned);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CollectionIsEmpty()
        {
            int[] collection = null;
            var returned = CollectionsUtilities.IntegerCollection(collection);
            Assert.Fail("No exception was thrown.");
        }
    }
}