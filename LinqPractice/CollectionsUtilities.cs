﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LinqPractice
{
    public class CollectionsUtilities
    {
        public static object FirstObject(object[] collection)
        {
            try
            {
                if (string.IsNullOrEmpty(collection[0].ToString()))
                { throw new ArgumentException(); }
            }
            catch (Exception) { throw; }
            var query = collection.First();
            return query;
        }

        public static int[] IntegerCollection(int[] integers)
        {
            if(integers==null) { throw new ArgumentNullException(); }
            var query = integers.Select(i => i * i)
                        .OrderBy(i => i).ToArray();
            return query;
        }
    }
}